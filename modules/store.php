<?php

require_once("config.php");
require_once("class_session.php");

session_start();


if(isset($_GET['logout'])) {
	session_destroy();
	header('Location: ../index.php');
}

$link = mysqli_connect(HOST, USER, PW);
if (!$link) {
  	die ("Error connecting to the database: " . mysqli_error());
}

$db_selected = mysqli_select_db($link,DB);
if (!$db_selected) {
  	die ("Error selecting the database: " . mysqli_error());
}

$error_flag = false;

include("header.html");

?>
	<div id="center">
		<div id="bar">
			<div id="login">
				<b>Login</b><br><br>
				<?php
				if(!empty($_POST) && !isset($_SESSION['username'])) {
					$username = mysqli_real_escape_string($link,$_POST['username']);
					$password = mysqli_real_escape_string($link, $_POST['password']);

					/* LIMIT 1: stop searching if you find a match */
					$query = "SELECT * FROM users WHERE username='".$username."' AND password='".$password."' LIMIT 1";
					$result = mysqli_query($link, $query);

					if(!$result) {
						die ("Query error $query: " . mysqli_error());
					}
					
					if(!mysqli_num_rows($result)) {
						mysqli_close($link);
						echo "Wrong username or password!";
						$error_flag = true;
					}
					else {
						/* Setup the SESSION */
						$_SESSION['username'] = $username;
						print "Logged as " .$_SESSION['username']; ?>
						<br><br>
						<a href="store.php?logout">Logout</a>			
				<?php
					}
				}
				else if(isset($_SESSION['username'])) {
					print "Logged as " .$_SESSION['username']; ?>
					<br><br>
					<a href="store.php?logout">Logout</a>
				<?php
				}
				else {
					session_regenerate_id();
					echo "Session Expired!";
					$error_flag = true;				
				}
				?>
			</div>
			<div id="cart">
				<b>Cart</b><br><br>
				<?php
				$query = "SELECT * FROM cart INNER JOIN products ON cart.id_item=products.id WHERE sid='" .$_SESSION['username']."'";
				$res = mysqli_query($link,$query) or die(mysqli_error());
				if(mysqli_num_rows($res) == 0) {
					echo "The cart is empty"; 
				}
				else {
					$t = 0;
					while($record = mysqli_fetch_assoc($res)) {?>
						<table>
						<tr>
						<tr><?php print $record['item']. " [" .$record['quantity']. "]";?></tr>		
						</tr>
						</table>
				<?php
					$t += $record['prize']*$record['quantity'];
					}?>
					<br><b><i>Total:</i></b>
				<?php
					print " " .$t;?>$<br>
					<br><hr><br>
					<b><i><a href="showcart.php">Show Cart</a></i></b>
				<?php
				}	
				?>	
			</div>
		</div>
		<div id="navigation">
                        <div id="pagenav">
			<?php 
                        if($error_flag == true) { ?>
				<a href="../index.php">Go back to the Login page</a>
			<?php 
			}
			else { ?>
				<b>Store</b><br><br>
				<table>
				<tr>	
				<td><b><i>Product</i></b></td>
				<td><b><i>Price</i></b></td>
				<td><b><i>Quantity</i></b></td>
				<td><b><i>Select</i></b></td>
				</tr>
				</table>
			<?php	
				/* Database query for the products */
				$query = "SELECT * FROM products";
				$products = mysqli_query($link, $query);

				if (!$products) {
  					die("Query error  $query: " . mysql_error());
				}
				
				while($product = mysqli_fetch_array($products) ) { ?>	
				<table>
				<tr>
				<td><?php print $product['item'];?></td>
				<td><?php print $product['prize'];?> $</td>
				<td>
				<form action="addcart.php" method="post">
				<input type="text" name="quantity[<?php print $product['id'];?>]" value="1" size="3">
				<input type="hidden" name="itemsid[]" value="<?php print $product['id'];?>">				
				</td>
				<td><input type="checkbox" name="sel[<?php print $product['id'];?>]" value="<?php print $product['item']?>"></td>
				</tr>
				</table>
			<?php
				}
			}
			mysqli_close($link);
                        if($error_flag == false) { ?>
			<br>
			<input type="submit" value="Add to Cart">
			</form>
			<?php } ?>
                        </div>
                </div>
	</div>
<?php

include("footer.html");

?>
