<?php

require_once("config.php");
require_once("class_session.php");

session_start();

if(isset($_GET['logout'])) {
	session_destroy();
	header('Location: ../index.php');
}

/* Get the parameters from the form */
$itemsid = $_POST['itemsid'];
$quantity = $_POST['quantity'];
$sel = $_POST['sel'];

$conn = mysqli_connect(HOST,USER,PW);
mysqli_select_db($conn, DB) or die(mysqli_error());

$error_log = false;

include("header.html");

?>
	<div id="center">
		<div id="bar">
                       <?php include("logstatus.php"); ?>
		</div>
		<div id="navigation">
                        <div id="pagenav">
				<?php
				/* Update the cart using the cart parameters */
				if(is_array($itemsid) && !empty($itemsid)) {
					$query = "LOCK TABLES cart WRITE";
					mysqli_query($conn, $query) or die(mysql_error());
					foreach($itemsid as $id) {
						$q = $quantity[$id];
						if(isset($sel[$id])) {
							$s = $sel[$id];
							$query = "SELECT * FROM cart WHERE sid='".$_SESSION['username']."' AND id_item=".$id;
							//$res = mysql_query($query,$conn) or die(mysql_error());
							$res = mysqli_query($conn, $query);
                                        		if(!$res) {
                                                		mysqli_query($conn,"UNLOCK TABLES");
                                                		mysqli_close($conn);
                                                		print mysqli_error();
                                        		}
							if(mysqli_num_rows($res) == 0) {
								$query ="INSERT INTO cart (sid,id_item,quantity) VALUES ('".$_SESSION['username']."',".$id.",".$q.")";
							}
							else {
								$query = "UPDATE cart SET quantity=quantity+".$q." WHERE sid='".$_SESSION['username']."' AND id_item=".$id;
							}
						//mysql_query($query,$conn) or die(mysql_error());
							$result = mysqli_query($conn, $query);
                                        		if(!$result) {
                                                		mysqli_query($conn,"UNLOCK TABLES");
                                                		mysql_close($conn);
                                                		print mysqli_error();
                                        		}
						}
					}
					$query = "UNLOCK TABLES";
					mysqli_query($conn, $query) or die(mysqli_error());
				}		
 				
				/* Login error */
                        	if($error_log == true) { ?>
					<a href="../index.php">Go back to the Login page</a>
				<?php
				}
				else {
					mysqli_close($conn);
					header('Location: store.php');
				}
				?> 
			</div>
                </div>
	</div>
<?php

include("footer.html");

?>
